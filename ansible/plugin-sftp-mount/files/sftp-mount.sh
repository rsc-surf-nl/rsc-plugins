#!/bin/bash

HOME_DIR="/home/${PAM_USER}"
SFTP_MOUNT_PATH=$(jq -r .sftp_mount_path < /etc/rsc/sftp_mount.json)

sftp_mount() {
  SFTP_TOKEN=$(jq -r .sftp_token < /etc/rsc/sftp_mount.json)
  DECODED_TOKEN=$(echo $SFTP_TOKEN | base64 -d)
  echo $DECODED_TOKEN |  jq -r .private_key_base64 | base64 -d > /etc/rsc/sftp_mount_key
  chown 600 /etc/rsc/sftp_mount_key
  SFTP_USER=$(echo $DECODED_TOKEN |  jq -r .username)
  SFTP_IP=$(echo $DECODED_TOKEN |  jq -r .ip)
  SFTP_URL=$SFTP_USER@$SFTP_IP

  apt install -y sshfs

  # Allow other users to mount FUSE
  sed -i 's/^#user_allow_other.*/user_allow_other/g' /etc/fuse.conf

  # Create remote user and data directory
  mkdir -p ~/.ssh
  ssh-keyscan -H "$SFTP_IP" >> ~/.ssh/known_hosts
  ssh $SFTP_URL -i /etc/rsc/sftp_mount_key "sudo mkdir -p $SFTP_MOUNT_PATH/$PAM_USER || true; sudo chmod -R 777 $SFTP_MOUNT_PATH/$PAM_USER || true"

  mkdir -p $HOME_DIR$SFTP_MOUNT_PATH || true
  chown -R $PAM_USER:$PAM_USER $HOME_DIR$SFTP_MOUNT_PATH

  grep -q "$HOME_DIR$SFTP_MOUNT_PATH" /etc/fstab || echo "sshfs#$SFTP_URL:$SFTP_MOUNT_PATH/$PAM_USER ${HOME_DIR}$SFTP_MOUNT_PATH fuse identityfile=/etc/rsc/sftp_mount_key,delay_connect,_netdev,defaults,user,allow_other 0 0" >> /etc/fstab
  sudo mount "$HOME_DIR$SFTP_MOUNT_PATH"
}

if ! mount | grep -q " ${HOME_DIR}$SFTP_MOUNT_PATH"; then
case "${PAM_TYPE}" in
  auth )
    if [ ! -f "${HOME_DIR}/sftp_mount.lock" ]; then
      touch "${HOME_DIR}/sftp_mount.lock"
      sftp_mount
      rm "${HOME_DIR}/sftp_mount.lock"
    fi
  ;;
  open_session )
    if [ ! -f "${HOME_DIR}/sftp_mount.lock" ]; then
      touch "${HOME_DIR}/sftp_mount.lock"
      sftp_mount
      rm "${HOME_DIR}/sftp_mount.lock"
    fi
  ;;
esac
fi