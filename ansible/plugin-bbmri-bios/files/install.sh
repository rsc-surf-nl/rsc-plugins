set -e

#Install wget
apt update
apt install --yes wget

echo "Install miniconda"
wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -b -p /etc/miniconda


/etc/miniconda/bin/conda init
. /etc/miniconda/bin/activate


echo "install dependencies"
conda env update --name base --file /etc/rsc/bbmri/env.yml


echo "install BBMRIomics"


export TAR="/bin/tar"
/etc/miniconda/bin/R -e "devtools::install_github('bbmri-nl/BBMRIomics', subdir='BBMRIomics', upgrade='never')"