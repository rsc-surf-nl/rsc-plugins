---
- name: Install and configure mysql-desktop
  hosts:
    - localhost
  gather_facts: false
  tasks:

    - name: create password for mysql root
      set_fact:
        mysql_root_pass: "{{ lookup('password', '/dev/null chars=ascii_letters,digits length=16') }}"

    - name: Specify MySQL root password before installing
      debconf: name='mysql-server' question='mysql-server/root_password' value='{{mysql_root_pass | quote}}' vtype='password'

    - name: Confirm MySQL root password before installing
      debconf: name='mysql-server' question='mysql-server/root_password_again' value='{{mysql_root_pass | quote}}' vtype='password'

    - name: Ensure required packages are installed
      package:
        name:
          - mysql-client
          - mysql-common
          - mysql-server
          - gcc
          - python3
          - python3-setuptools
          - python3-pip
        state: latest

    - name: Install the MySQL-python through pip
      pip:
        name: PyMySQL

    - name: Ensure mysql service is running
      systemd:
        name: mysql
        state: restarted
        enabled: true

    - name: Delete file before copy
      file:
        path: /etc/mysql/.my.cnf
        state: absent

    - name: Delete file before copy
      file:
        path: /etc/mysql/my.cnf
        state: absent

    - name: Delete file before copy
      file:
        path: /etc/my.cnf
        state: absent

    - name: Add .my.cnf to /etc/mysql
      template:
        src: my.cnf.j2
        dest: /etc/mysql/.my.cnf

    - name: Add my.cnf to /etc/mysql
      template:
        src: my.cnf.j2
        dest: /etc/mysql/my.cnf

    - name: Add my.cnf to /etc/
      template:
        src: my.cnf.j2
        dest: /etc/my.cnf

    - name: Disallow root login remotely
      command: |
        mysql -p{{ mysql_root_pass }} -ne "{{ item }}"
      with_items:
        - DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')
      changed_when: false
      ignore_errors: true

# mysql configuration

    - name: change bind-address
      lineinfile:
        path: /etc/mysql/mysql.conf.d/mysqld.cnf
        regexp: "^bind-address"
        line: bind-address = 0.0.0.0

    - name: change mysqlx-bind-address
      lineinfile:
        path: /etc/mysql/mysql.conf.d/mysqld.cnf
        regexp: "^mysqlx-bind-address"
        line: mysqlx-bind-address = 0.0.0.0

# mysql-workbench
    - name: copy mysql-workbench-community deb
      copy:
        src: mysql-workbench-community_8.0.27-1ubuntu20.04_amd64.deb
        dest: /etc/mysql-workbench-community_8.0.27-1ubuntu20.04_amd64.deb

    - name: Install mysql-workbench-community
      apt: deb="/etc/mysql-workbench-community_8.0.27-1ubuntu20.04_amd64.deb"

# phpMyAdmin
    - name: debconf for pma
      debconf: name=phpmyadmin question='phpmyadmin/dbconfig-install' value='true' vtype='boolean'

    - name: debconf for pma
      debconf: name=phpmyadmin question='phpmyadmin/app-password-confirm' value='{{mysql_root_pass}}' vtype='password'

    - name: debconf for pma
      debconf: name=phpmyadmin question='phpmyadmin/mysql/admin-pass' value='{{mysql_root_pass}}' vtype='password'

    - name: debconf for pma
      debconf: name=phpmyadmin question='phpmyadmin/mysql/app-pass' value='{{mysql_root_pass}}' vtype='password'

    - name: debconf for pma
      debconf: name=phpmyadmin question='phpmyadmin/reconfigure-webserver' value='' vtype='multiselect'

    - name: Ensure required packages are installed
      package:
        name:
          - phpmyadmin
          - php7.4-fpm
        state: latest

    - name: Create nginx location block for phpmyadmin
      copy:
        src: files/nginx/phpmyadmin.conf
        dest: /etc/nginx/app-location-conf.d/phpmyadmin.conf
        mode: 0644

    - name: Restart nginx
      service:
        name: nginx
        state: restarted

    - name: Restart Mysql
      systemd:
        name: mysql
        state: restarted
