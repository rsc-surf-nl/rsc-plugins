
$LOGFILE = "c:\logs\plugin-windows-anaconda.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

Function Main {

  Write-Log "Start plugin-windows-anaconda"
 
  try {
     choco feature enable -n allowGlobalConfirmation
      #install anaconda
      If(Test-Path -Path "$env:ProgramData\Chocolatey") {
        choco install anaconda3  --no-progress --params '"/AddToPath /D:C:\ProgramData\Chocolatey\tools"'
      }
      Else {
        Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
        choco install anaconda3  --no-progress --params '"/AddToPath /D:C:\ProgramData\Chocolatey\tools"'
      }
  }
  catch {
      Write-Log "$_"
      Throw $_
  }
  
  Write-Log "End plugin-windows-anaconda"
 
}

Main    

